// Signature format: 4.0
package androidx.appsearch.app {

  @RequiresApi(api=android.os.Build.VERSION_CODES.LOLLIPOP) public class ShortcutAdapter {
    method public static androidx.core.content.pm.ShortcutInfoCompat.Builder createShortcutBuilderFromDocument(android.content.Context, Object) throws androidx.appsearch.exceptions.AppSearchException;
    method public static android.net.Uri getDocumentUri(Object) throws androidx.appsearch.exceptions.AppSearchException;
    method public static android.net.Uri getDocumentUri(String);
    field public static final String DEFAULT_NAMESPACE = "__shortcut_adapter_ns__";
  }

}

package androidx.appsearch.builtintypes {

  @androidx.appsearch.annotation.Document(name="builtin:Timer") public class Timer {
    method public long getCreationTimestampMillis();
    method public long getDurationMillis();
    method public String getId();
    method public String? getName();
    method public String getNamespace();
    method public long getRemainingTimeMillis();
    method public String? getRingtone();
    method public int getScore();
    method public long getStartTimeMillis();
    method public long getStartTimeMillisInElapsedRealtime();
    method public int getStatus();
    method public long getTtlMillis();
    method public boolean isVibrate();
    field public static final int STATUS_EXPIRED = 3; // 0x3
    field public static final int STATUS_MISSED = 4; // 0x4
    field public static final int STATUS_PAUSED = 2; // 0x2
    field public static final int STATUS_RESET = 5; // 0x5
    field public static final int STATUS_STARTED = 1; // 0x1
    field public static final int STATUS_UNKNOWN = 0; // 0x0
  }

  public static final class Timer.Builder {
    ctor public Timer.Builder(String, String);
    ctor public Timer.Builder(androidx.appsearch.builtintypes.Timer);
    method public androidx.appsearch.builtintypes.Timer build();
    method public androidx.appsearch.builtintypes.Timer.Builder setCreationTimestampMillis(long);
    method public androidx.appsearch.builtintypes.Timer.Builder setDurationMillis(long);
    method public androidx.appsearch.builtintypes.Timer.Builder setName(String?);
    method public androidx.appsearch.builtintypes.Timer.Builder setRemainingTimeMillis(long);
    method public androidx.appsearch.builtintypes.Timer.Builder setRingtone(String?);
    method public androidx.appsearch.builtintypes.Timer.Builder setScore(int);
    method public androidx.appsearch.builtintypes.Timer.Builder setStartTimeMillis(long);
    method public androidx.appsearch.builtintypes.Timer.Builder setStartTimeMillisInElapsedRealtime(long);
    method public androidx.appsearch.builtintypes.Timer.Builder setStatus(int);
    method public androidx.appsearch.builtintypes.Timer.Builder setTtlMillis(long);
    method public androidx.appsearch.builtintypes.Timer.Builder setVibrate(boolean);
  }

}

